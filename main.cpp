#include "ascenseur.hpp"
#include <iostream>

int main() {
    Ascenseur ascenseur;
    int choix, etage, nouveau_passager; // déclaration de la variable nouveau_passager

    do {
        std::cout << "Que voulez-vous faire ?" << std::endl;
        std::cout << "1. Appeler l'ascenseur" << std::endl;
        std::cout << "2. Aller a quel etage" << std::endl;
        std::cout << "3. Afficher etat de l'ascenseur" << std::endl;
        std::cout << "4. Quitter" << std::endl;
        std::cout << "> ";
        std::cin >> choix;

        switch (choix) {
            case 1:
                std::cout << "A quel etages-vous ? ";
                std::cin >> etage;
                ascenseur.appel(etage);
                break;
            case 2:
                std::cout << "Ou voulez-vous aller ? ";
                std::cin >> etage;
                ascenseur.aller_a(etage);
                std::cout << "Combien de passagers entrent dans l'ascenseur ? ";
                std::cin >> nouveau_passager;
                ascenseur.monter(nouveau_passager);
                break;
            case 3:
                ascenseur.afficher_etat();
                break;
            case 4:
                std::cout << "Au revoir !" << std::endl;
                break;
            default:
                std::cout << "Choix invalide." << std::endl;
        }
        if (choix != 4) {
            ascenseur.deplacer();
        }
    } while (choix != 4);

    return 0;
}