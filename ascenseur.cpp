#include "ascenseur.hpp"
#include <iostream>

Ascenseur::Ascenseur() {
    etage_actuel = 0;
    nb_passagers = 0;
    capacite_max = 8;
    for (int i = 0; i < 10; i++) {
        bouton_appui[i] = false;
        nb_personnes_etage[i] = 0;
    }
    direction = 0;
}

void Ascenseur::appel(int etage) {
    bouton_appui[etage] = true;
    nb_personnes_etage[etage]++;
    if (direction == 0) {
        // si l'ascenseur est immobile, il se dirige vers l'étage demandé
        direction = (etage > etage_actuel) ? 1 : -1;
    }
}

void Ascenseur::aller_a(int etage) {
    bouton_appui[etage] = false;
    direction = (etage > etage_actuel) ? 1 : -1;
}

void Ascenseur::deplacer() {
    etage_actuel += direction;
    if (etage_actuel == 0 || etage_actuel == 9) {
        // l'ascenseur arrive au RDC ou au dernier étage, il change de direction
        direction *= -1;
    }
    if (bouton_appui[etage_actuel]) {
        // l'ascenseur s'arrête à l'étage demandé
        bouton_appui[etage_actuel] = false;
        nb_personnes_etage[etage_actuel] -= capacite_max - nb_passagers;
        nb_passagers = capacite_max;
        direction = 0;
    }
}

void Ascenseur::afficher_etat() {
    std::cout << "Etage actuel : " << etage_actuel << std::endl;
    for (int i = 0; i < 10; i++) {
        std::cout << "Palier " << i << " : " << nb_personnes_etage[i] << " personnes" << std::endl;
    }
    std::cout << "Ascenseur : " << nb_passagers << " personnes" << std::endl;
}

void Ascenseur::monter(int nb_personnes) {
    if (nb_passagers + nb_personnes <= capacite_max) {
        nb_passagers += nb_personnes;
        direction = 0;
    }
}