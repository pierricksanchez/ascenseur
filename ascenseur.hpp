#ifndef ASCENSEUR_HPP
#define ASCENSEUR_HPP

class Ascenseur {
public:
    Ascenseur();
    void appel(int etage);
    void aller_a(int etage);
    void deplacer();
    void afficher_etat();
    void monter(int nb_personne);
    // int get_etage_actuel() const; // fonction publique pour accéder à etage_actuel
    // int get_direction() const; // fonction publique pour accéder à direction
private:
    int etage_actuel;
    int nb_passagers;
    int capacite_max;
    int direction; // direction doit être déclarée ici car elle est utilisée dans appel()
    bool bouton_appui[10]; // tableau pour stocker les boutons d'appel
    int nb_personnes_etage[8]; // tableau pour stocker le nombre de personnes sur chaque palier
};

#endif